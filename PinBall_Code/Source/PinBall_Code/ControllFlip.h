// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Flipper.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ControllFlip.generated.h"

/**
 * 
 */
UCLASS()
class PINBALL_CODE_API AControllFlip : public APlayerController
{
	GENERATED_BODY()
	
public:
	// Sets default values for this pawn's properties
	AControllFlip();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:
	virtual void SetupInputComponent() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Element)
		TArray<AActor*>RightFlipper;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Element)
		TArray<AActor*>LeftFlipper;
	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
		AActor* Flip;
	UPROPERTY(Transient, EditAnywhere, BlueprintReadWrite)
		const TSubclassOf<AActor> Flipp;

	UFUNCTION()
		void Flipper();
	UFUNCTION()
		void RightPress();
	UFUNCTION()
		void LeftPress();
};
