// Fill out your copyright notice in the Description page of Project Settings.

#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Flipper.h"

// Sets default values
AFlipper::AFlipper()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Flip = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Flip"));
	RootComponent = Flip;
}

// Called when the game starts or when spawned
void AFlipper::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFlipper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFlipper::Active()
{
	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	UKismetSystemLibrary::MoveComponentTo(Flip, GetActorLocation(), StartRotation, false, false, 0.2f, false, EMoveComponentAction::Type::Move, LatentInfo);
}

void AFlipper::Deactive()
{
	FLatentActionInfo LatentInfo;
	LatentInfo.CallbackTarget = this;
	UKismetSystemLibrary::MoveComponentTo(Flip, GetActorLocation(), EndRotation, false, false, 0.2f, false, EMoveComponentAction::Type::Move, LatentInfo);
}

