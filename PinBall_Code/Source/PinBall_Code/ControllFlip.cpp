// Fill out your copyright notice in the Description page of Project Settings.

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ControllFlip.h"

AControllFlip::AControllFlip()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AControllFlip::BeginPlay()
{
	Super::BeginPlay();
	Flipper();
}

void AControllFlip::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("RightFlipper", IE_Pressed, this, &AControllFlip::RightPress);
	InputComponent->BindAction("LeftFlipper", IE_Pressed, this, &AControllFlip::LeftPress);
}


void AControllFlip::Flipper()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), Flipp, FoundActors);
	UE_LOG(LogTemp, Warning, TEXT("%d"), FoundActors.Num());
	for (int i = 0; i < FoundActors.Num(); i++) {

		switch (Cast<AFlipper>(FoundActors[i])->TypeFlipper)
		{
			case Right:
			{
				RightFlipper.Add(FoundActors[i]);
				UE_LOG(LogTemp, Warning, TEXT("ActorRight"));
				break;
			}
			case Left:
			{
				LeftFlipper.Add(FoundActors[i]);
				UE_LOG(LogTemp, Warning, TEXT("ActorLeft"));
				break;
			}
			
		}
	}
}

void AControllFlip::RightPress()
{
	
	AFlipper* Temp;

	for (int i = 0; i < RightFlipper.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("InputRight"));
		Temp = Cast<AFlipper>(RightFlipper[i]);
		if (Temp)
		{
			Temp->Active();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("NoTemp"));
		}
	}
}

void AControllFlip::LeftPress()
{
	AFlipper* LeftFlip;

	for (int i = 0; i < LeftFlipper.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("InputLeft"));
		LeftFlip = Cast<AFlipper>(LeftFlipper[i]);
		if (LeftFlip)
		{
			LeftFlip->Active();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("NoTemp"));
		}
	}
}
