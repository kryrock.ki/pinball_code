// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PinBall_CodeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PINBALL_CODE_API APinBall_CodeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
