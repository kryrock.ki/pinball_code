// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Flipper.generated.h"

UENUM()
enum TypeFlipper
{
	Right       UMETA(DisplayName = "Right"),
	Left      UMETA(DisplayName = "Left"),
};
UCLASS()
class PINBALL_CODE_API AFlipper : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFlipper();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Flip;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		TEnumAsByte <TypeFlipper> TypeFlipper;
	UPROPERTY(EditAnywhere)
		FRotator StartRotation;
	UPROPERTY(EditAnywhere)
		FRotator EndRotation;

	UFUNCTION()
		void Active();
	UFUNCTION()
		void Deactive();
};
