// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PINBALL_CODE_PinBall_CodeGameModeBase_generated_h
#error "PinBall_CodeGameModeBase.generated.h already included, missing '#pragma once' in PinBall_CodeGameModeBase.h"
#endif
#define PINBALL_CODE_PinBall_CodeGameModeBase_generated_h

#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_SPARSE_DATA
#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_RPC_WRAPPERS
#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPinBall_CodeGameModeBase(); \
	friend struct Z_Construct_UClass_APinBall_CodeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APinBall_CodeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(APinBall_CodeGameModeBase)


#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPinBall_CodeGameModeBase(); \
	friend struct Z_Construct_UClass_APinBall_CodeGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APinBall_CodeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(APinBall_CodeGameModeBase)


#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APinBall_CodeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APinBall_CodeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinBall_CodeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinBall_CodeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinBall_CodeGameModeBase(APinBall_CodeGameModeBase&&); \
	NO_API APinBall_CodeGameModeBase(const APinBall_CodeGameModeBase&); \
public:


#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APinBall_CodeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APinBall_CodeGameModeBase(APinBall_CodeGameModeBase&&); \
	NO_API APinBall_CodeGameModeBase(const APinBall_CodeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APinBall_CodeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APinBall_CodeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APinBall_CodeGameModeBase)


#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_12_PROLOG
#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_RPC_WRAPPERS \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_INCLASS \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PINBALL_CODE_API UClass* StaticClass<class APinBall_CodeGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PinBall_Code_Source_PinBall_Code_PinBall_CodeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
