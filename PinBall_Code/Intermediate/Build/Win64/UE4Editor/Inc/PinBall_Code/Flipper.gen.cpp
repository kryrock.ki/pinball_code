// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PinBall_Code/Flipper.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFlipper() {}
// Cross Module References
	PINBALL_CODE_API UEnum* Z_Construct_UEnum_PinBall_Code_TypeFlipper();
	UPackage* Z_Construct_UPackage__Script_PinBall_Code();
	PINBALL_CODE_API UClass* Z_Construct_UClass_AFlipper_NoRegister();
	PINBALL_CODE_API UClass* Z_Construct_UClass_AFlipper();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static UEnum* TypeFlipper_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PinBall_Code_TypeFlipper, Z_Construct_UPackage__Script_PinBall_Code(), TEXT("TypeFlipper"));
		}
		return Singleton;
	}
	template<> PINBALL_CODE_API UEnum* StaticEnum<TypeFlipper>()
	{
		return TypeFlipper_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_TypeFlipper(TypeFlipper_StaticEnum, TEXT("/Script/PinBall_Code"), TEXT("TypeFlipper"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PinBall_Code_TypeFlipper_Hash() { return 2063477153U; }
	UEnum* Z_Construct_UEnum_PinBall_Code_TypeFlipper()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PinBall_Code();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("TypeFlipper"), 0, Get_Z_Construct_UEnum_PinBall_Code_TypeFlipper_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "Right", (int64)Right },
				{ "Left", (int64)Left },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Left.DisplayName", "Left" },
				{ "Left.Name", "Left" },
				{ "ModuleRelativePath", "Flipper.h" },
				{ "Right.DisplayName", "Right" },
				{ "Right.Name", "Right" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PinBall_Code,
				nullptr,
				"TypeFlipper",
				"TypeFlipper",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(AFlipper::execDeactive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Deactive();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AFlipper::execActive)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Active();
		P_NATIVE_END;
	}
	void AFlipper::StaticRegisterNativesAFlipper()
	{
		UClass* Class = AFlipper::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Active", &AFlipper::execActive },
			{ "Deactive", &AFlipper::execDeactive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFlipper_Active_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFlipper_Active_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFlipper_Active_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFlipper, nullptr, "Active", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFlipper_Active_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFlipper_Active_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFlipper_Active()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFlipper_Active_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFlipper_Deactive_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFlipper_Deactive_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFlipper_Deactive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFlipper, nullptr, "Deactive", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFlipper_Deactive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFlipper_Deactive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFlipper_Deactive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFlipper_Deactive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFlipper_NoRegister()
	{
		return AFlipper::StaticClass();
	}
	struct Z_Construct_UClass_AFlipper_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StartRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TypeFlipper_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TypeFlipper;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Flip_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Flip;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFlipper_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PinBall_Code,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFlipper_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFlipper_Active, "Active" }, // 1769613911
		{ &Z_Construct_UFunction_AFlipper_Deactive, "Deactive" }, // 2271971289
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFlipper_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Flipper.h" },
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFlipper_Statics::NewProp_EndRotation_MetaData[] = {
		{ "Category", "Flipper" },
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AFlipper_Statics::NewProp_EndRotation = { "EndRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFlipper, EndRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_AFlipper_Statics::NewProp_EndRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::NewProp_EndRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFlipper_Statics::NewProp_StartRotation_MetaData[] = {
		{ "Category", "Flipper" },
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AFlipper_Statics::NewProp_StartRotation = { "StartRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFlipper, StartRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_AFlipper_Statics::NewProp_StartRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::NewProp_StartRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFlipper_Statics::NewProp_TypeFlipper_MetaData[] = {
		{ "Category", "Status" },
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AFlipper_Statics::NewProp_TypeFlipper = { "TypeFlipper", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFlipper, TypeFlipper), Z_Construct_UEnum_PinBall_Code_TypeFlipper, METADATA_PARAMS(Z_Construct_UClass_AFlipper_Statics::NewProp_TypeFlipper_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::NewProp_TypeFlipper_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFlipper_Statics::NewProp_Flip_MetaData[] = {
		{ "Category", "Flipper" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Flipper.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFlipper_Statics::NewProp_Flip = { "Flip", nullptr, (EPropertyFlags)0x0010000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFlipper, Flip), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFlipper_Statics::NewProp_Flip_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::NewProp_Flip_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFlipper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFlipper_Statics::NewProp_EndRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFlipper_Statics::NewProp_StartRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFlipper_Statics::NewProp_TypeFlipper,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFlipper_Statics::NewProp_Flip,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFlipper_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFlipper>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFlipper_Statics::ClassParams = {
		&AFlipper::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AFlipper_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFlipper_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFlipper_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFlipper()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFlipper_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFlipper, 1769136070);
	template<> PINBALL_CODE_API UClass* StaticClass<AFlipper>()
	{
		return AFlipper::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFlipper(Z_Construct_UClass_AFlipper, &AFlipper::StaticClass, TEXT("/Script/PinBall_Code"), TEXT("AFlipper"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFlipper);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
