// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PINBALL_CODE_Flipper_generated_h
#error "Flipper.generated.h already included, missing '#pragma once' in Flipper.h"
#endif
#define PINBALL_CODE_Flipper_generated_h

#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_SPARSE_DATA
#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDeactive); \
	DECLARE_FUNCTION(execActive);


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDeactive); \
	DECLARE_FUNCTION(execActive);


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFlipper(); \
	friend struct Z_Construct_UClass_AFlipper_Statics; \
public: \
	DECLARE_CLASS(AFlipper, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(AFlipper)


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAFlipper(); \
	friend struct Z_Construct_UClass_AFlipper_Statics; \
public: \
	DECLARE_CLASS(AFlipper, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(AFlipper)


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFlipper(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFlipper) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFlipper); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFlipper); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFlipper(AFlipper&&); \
	NO_API AFlipper(const AFlipper&); \
public:


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFlipper(AFlipper&&); \
	NO_API AFlipper(const AFlipper&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFlipper); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFlipper); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFlipper)


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_PRIVATE_PROPERTY_OFFSET
#define PinBall_Code_Source_PinBall_Code_Flipper_h_15_PROLOG
#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_RPC_WRAPPERS \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_INCLASS \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PinBall_Code_Source_PinBall_Code_Flipper_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_INCLASS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_Flipper_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PINBALL_CODE_API UClass* StaticClass<class AFlipper>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PinBall_Code_Source_PinBall_Code_Flipper_h


#define FOREACH_ENUM_TYPEFLIPPER(op) \
	op(Right) \
	op(Left) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
