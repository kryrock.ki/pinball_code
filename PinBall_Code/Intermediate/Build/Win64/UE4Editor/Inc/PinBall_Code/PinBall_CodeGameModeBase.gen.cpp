// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PinBall_Code/PinBall_CodeGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePinBall_CodeGameModeBase() {}
// Cross Module References
	PINBALL_CODE_API UClass* Z_Construct_UClass_APinBall_CodeGameModeBase_NoRegister();
	PINBALL_CODE_API UClass* Z_Construct_UClass_APinBall_CodeGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PinBall_Code();
// End Cross Module References
	void APinBall_CodeGameModeBase::StaticRegisterNativesAPinBall_CodeGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_APinBall_CodeGameModeBase_NoRegister()
	{
		return APinBall_CodeGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APinBall_CodeGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PinBall_Code,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PinBall_CodeGameModeBase.h" },
		{ "ModuleRelativePath", "PinBall_CodeGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APinBall_CodeGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::ClassParams = {
		&APinBall_CodeGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APinBall_CodeGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APinBall_CodeGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APinBall_CodeGameModeBase, 619247763);
	template<> PINBALL_CODE_API UClass* StaticClass<APinBall_CodeGameModeBase>()
	{
		return APinBall_CodeGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APinBall_CodeGameModeBase(Z_Construct_UClass_APinBall_CodeGameModeBase, &APinBall_CodeGameModeBase::StaticClass, TEXT("/Script/PinBall_Code"), TEXT("APinBall_CodeGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APinBall_CodeGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
