// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PinBall_Code/ControllFlip.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeControllFlip() {}
// Cross Module References
	PINBALL_CODE_API UClass* Z_Construct_UClass_AControllFlip_NoRegister();
	PINBALL_CODE_API UClass* Z_Construct_UClass_AControllFlip();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_PinBall_Code();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AControllFlip::execLeftPress)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LeftPress();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControllFlip::execRightPress)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RightPress();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AControllFlip::execFlipper)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Flipper();
		P_NATIVE_END;
	}
	void AControllFlip::StaticRegisterNativesAControllFlip()
	{
		UClass* Class = AControllFlip::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Flipper", &AControllFlip::execFlipper },
			{ "LeftPress", &AControllFlip::execLeftPress },
			{ "RightPress", &AControllFlip::execRightPress },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AControllFlip_Flipper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControllFlip_Flipper_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControllFlip_Flipper_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControllFlip, nullptr, "Flipper", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControllFlip_Flipper_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControllFlip_Flipper_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControllFlip_Flipper()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControllFlip_Flipper_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControllFlip_LeftPress_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControllFlip_LeftPress_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControllFlip_LeftPress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControllFlip, nullptr, "LeftPress", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControllFlip_LeftPress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControllFlip_LeftPress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControllFlip_LeftPress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControllFlip_LeftPress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AControllFlip_RightPress_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AControllFlip_RightPress_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AControllFlip_RightPress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AControllFlip, nullptr, "RightPress", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AControllFlip_RightPress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AControllFlip_RightPress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AControllFlip_RightPress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AControllFlip_RightPress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AControllFlip_NoRegister()
	{
		return AControllFlip::StaticClass();
	}
	struct Z_Construct_UClass_AControllFlip_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Flipp_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Flipp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Flip_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Flip;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftFlipper_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LeftFlipper;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LeftFlipper_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightFlipper_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RightFlipper;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RightFlipper_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AControllFlip_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_PinBall_Code,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AControllFlip_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AControllFlip_Flipper, "Flipper" }, // 1899754940
		{ &Z_Construct_UFunction_AControllFlip_LeftPress, "LeftPress" }, // 3080445986
		{ &Z_Construct_UFunction_AControllFlip_RightPress, "RightPress" }, // 555406235
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControllFlip_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ControllFlip.h" },
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControllFlip_Statics::NewProp_Flipp_MetaData[] = {
		{ "Category", "ControllFlip" },
		{ "ModuleRelativePath", "ControllFlip.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_Flipp = { "Flipp", nullptr, (EPropertyFlags)0x0014000000002005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControllFlip, Flipp), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AControllFlip_Statics::NewProp_Flipp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::NewProp_Flipp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControllFlip_Statics::NewProp_Flip_MetaData[] = {
		{ "Category", "ControllFlip" },
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_Flip = { "Flip", nullptr, (EPropertyFlags)0x0010000000002005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControllFlip, Flip), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AControllFlip_Statics::NewProp_Flip_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::NewProp_Flip_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper_MetaData[] = {
		{ "Category", "Element" },
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper = { "LeftFlipper", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControllFlip, LeftFlipper), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper_Inner = { "LeftFlipper", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper_MetaData[] = {
		{ "Category", "Element" },
		{ "ModuleRelativePath", "ControllFlip.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper = { "RightFlipper", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AControllFlip, RightFlipper), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper_Inner = { "RightFlipper", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AControllFlip_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_Flipp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_Flip,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_LeftFlipper_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AControllFlip_Statics::NewProp_RightFlipper_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AControllFlip_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AControllFlip>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AControllFlip_Statics::ClassParams = {
		&AControllFlip::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AControllFlip_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AControllFlip_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AControllFlip_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AControllFlip()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AControllFlip_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AControllFlip, 1456370625);
	template<> PINBALL_CODE_API UClass* StaticClass<AControllFlip>()
	{
		return AControllFlip::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AControllFlip(Z_Construct_UClass_AControllFlip, &AControllFlip::StaticClass, TEXT("/Script/PinBall_Code"), TEXT("AControllFlip"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AControllFlip);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
