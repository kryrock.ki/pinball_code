// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PINBALL_CODE_ControllFlip_generated_h
#error "ControllFlip.generated.h already included, missing '#pragma once' in ControllFlip.h"
#endif
#define PINBALL_CODE_ControllFlip_generated_h

#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_SPARSE_DATA
#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLeftPress); \
	DECLARE_FUNCTION(execRightPress); \
	DECLARE_FUNCTION(execFlipper);


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLeftPress); \
	DECLARE_FUNCTION(execRightPress); \
	DECLARE_FUNCTION(execFlipper);


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAControllFlip(); \
	friend struct Z_Construct_UClass_AControllFlip_Statics; \
public: \
	DECLARE_CLASS(AControllFlip, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(AControllFlip)


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAControllFlip(); \
	friend struct Z_Construct_UClass_AControllFlip_Statics; \
public: \
	DECLARE_CLASS(AControllFlip, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PinBall_Code"), NO_API) \
	DECLARE_SERIALIZER(AControllFlip)


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AControllFlip(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AControllFlip) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControllFlip); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControllFlip); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControllFlip(AControllFlip&&); \
	NO_API AControllFlip(const AControllFlip&); \
public:


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AControllFlip(AControllFlip&&); \
	NO_API AControllFlip(const AControllFlip&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AControllFlip); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AControllFlip); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AControllFlip)


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_PRIVATE_PROPERTY_OFFSET
#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_13_PROLOG
#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_RPC_WRAPPERS \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_INCLASS \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_PRIVATE_PROPERTY_OFFSET \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_SPARSE_DATA \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_INCLASS_NO_PURE_DECLS \
	PinBall_Code_Source_PinBall_Code_ControllFlip_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PINBALL_CODE_API UClass* StaticClass<class AControllFlip>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PinBall_Code_Source_PinBall_Code_ControllFlip_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
